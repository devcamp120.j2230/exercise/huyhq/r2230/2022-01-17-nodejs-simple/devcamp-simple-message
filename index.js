//khai báo thư viện express
const { response } = require("express");
const express = require("express");

//khởi tạo app NodeJs
const  app = express();

//khai baos cổng để chạy ứng dụng
const port = 8000;

//khai báo middleware
app.use(express.json());

//khai báo Get Api 
app.get('/', (request, response)=>{
    let date = new Date();
    console.log(date.getTime());

    response.status(200).json({
        message :  `Hôm nay là ngày ${date.getDate()} tháng ${date.getMonth()+1}`,
    })
})

//khai báo Post Api
app.post('/', (request, response)=>{
    let mes = "Post Method";
    console.log(mes);

    response.status(200).json({
        message :  mes,
    })
})

//khai báo Put Api
app.put('/', (request, response)=>{
    let mes = "Put Method";
    console.log(mes);

    response.status(200).json({
        message :  mes,
    })
})

//khai báo Delete Api
app.delete('/', (request, response)=>{
    let mes = "Delete Method";
    console.log(mes);

    response.status(200).json({
        message :  mes,
    })
})

//Lấy tham số trên Url
app.get('/example/:name/:age', (request, response)=>{
    let name = request.params.name;
    let age = request.params.age;
    console.log('Get Method Param');

    response.status(200).json({
        name,
        age,
    })
})

//Lấy tham số query string trên Url
app.get('/example2', (request, response)=>{
    let query = request.query;
    
    console.log('Get Method Query string');

    response.status(200).json({
        query
    })
})

//Lấy tham request body json
app.get('/example3', (request, response)=>{
    let body = request.body;
    
    console.log('Get Method Body object');

    response.status(200).json({
        body
    })
})


//mở ứng dụng trên cổng
app.listen(port, ()=>{
    console.log("App running on port: "+port);
})
